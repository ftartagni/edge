#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include "DHT.h"
#define DHTPIN D6   // Digital pin connected to the DHT sensor
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);
float temperature;
/* SET YOUR OWN ssid, pwd and IP address ! */
/* wifi network name */
char* ssidName = "FritzFlop"; /*WifiNameExample*/
/* WPA2 PSK password */
char* pwd = "ExamplePassword123"; /*ExamplePassword123*/
/* service IP address */ 
char* address = "http://192.168.1.28:8080";

void setup() { 
  Serial.begin(115200);                                
  WiFi.begin(ssidName, pwd);
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
    Serial.print(".");
  } 
  Serial.println("Connected: \n local IP: "+WiFi.localIP());
  pinMode(DHTPIN, INPUT);
  dht.begin(); 
}

int sendData(String address, float value, float place){  
   HTTPClient http;    
   http.begin(address + "/api/data");      
   http.addHeader("Content-Type", "application/json");     
   String msg = 
    String("{ \"value\": ") + value + 
    ", \"place\": \"" + place +"\" }";
   int retCode = http.POST(msg);   
   http.end();  
   
   return retCode;
}
   
void loop() { 
 if (WiFi.status()== WL_CONNECTED){   
    float value = dht.readTemperature();
    float hum = dht.readHumidity();
    Serial.println("Sending humidity: " + String(hum));
   /* send data */
   Serial.println("Sending temperature: "+String(value)+"...");    
   int code = sendData(address, value, hum);

   /* log result */
   if (code == 200){
     Serial.println("ok");   
   } else {
     Serial.println("error");
   }
 } else { 
   Serial.println("Error in WiFi connection");   
 }
 
 delay(4000);  
 
}
